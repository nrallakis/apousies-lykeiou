package rallakis.nicholas.apousieslykeiou;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;

public class AdManager {

    private static final String AD_UNIT_ID = "ca-app-pub-7739991390722327/4267633175";
    private static final String TEST = "ca-app-pub-3940256099942544/1033173712";

    private static AdManager mInstance;

    private InterstitialAd mInterstitialAd;

    private boolean hasShownAdOneTime;
    private int count;

    private AdManager(Context c) {
        // Set user property once the app starts for once.
        FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(c);
        String currentTimeInSeconds = String.valueOf(System.currentTimeMillis() / 1000);
        analytics.setUserProperty("app_open", currentTimeInSeconds);

        mInterstitialAd = new InterstitialAd(c);
        mInterstitialAd.setAdUnitId(AD_UNIT_ID);
        loadAd();

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                loadAd();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("AD", "AD FAILED");
            }
        });
    }

    private void loadAd() {
        Log.d("AD", "LOAD AD");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    public static AdManager getInstance(Context c) {
        if (mInstance == null) {
            mInstance = new AdManager(c);
        }
        return mInstance;
    }

    public void incrementMoves() {
        count++;
    }

    public boolean shouldShowAd() {
        if (!mInterstitialAd.isLoaded()) return false;
        if (hasShownAdOneTime) {
            return count == 4;
        }
        return count == 1;
    }

    public void showAds() {
        Log.d("AD", "SHOW AD");
        hasShownAdOneTime = true;
        count = 0;
        mInterstitialAd.show();
    }
}
