package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;

import java.util.ArrayList;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.helper.Utils;


public class AbsenceHoursDialogFragment extends DialogFragment {

    public static final String TAG = "absence_hours_dialog";

    public static final String EXTRA_HOURS = "extra_hours";
    private static final String KEY_HOURS = "hours";
    private static final String KEY_EXCLUDED_HOURS = "hours_excluded";

    private boolean[] mHoursAbsent;
    private String[] mItems;

    public AbsenceHoursDialogFragment() {}

    public static AbsenceHoursDialogFragment newInstance(boolean[] hoursSelected, boolean[] hoursExcluded) {
        Bundle args = new Bundle();
        args.putBooleanArray(KEY_HOURS, hoursSelected);
        args.putBooleanArray(KEY_EXCLUDED_HOURS, hoursExcluded);

        AbsenceHoursDialogFragment fragment = new AbsenceHoursDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mHoursAbsent = getArguments().getBooleanArray(KEY_HOURS);
        boolean[] hoursExcluded = getArguments().getBooleanArray(KEY_EXCLUDED_HOURS);

        mItems = createItems(hoursExcluded);

        boolean[] itemsChecked = new boolean[mItems.length];
        for (int i = 0; i < mItems.length; i++) {
            int realIndex = Integer.parseInt(mItems[i].substring(0,1))-1;
            itemsChecked[i] = mHoursAbsent[realIndex];
        }

        AlertDialog.Builder builder = buildDialog(hoursExcluded, itemsChecked);

        return builder.create();
    }

    private String[] createItems(boolean[] hoursExcluded) {
        String[] allItems = new String[hoursExcluded.length];
        for (int i = 0; i < allItems.length; i++) {
            allItems[i] = (i+1) + "η";
        }

        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < hoursExcluded.length; i++) {
            if (!hoursExcluded[i]) {
                temp.add(allItems[i]);
            }
        }

        String[] arr = new String[temp.size()];
        arr = temp.toArray(arr);
        return arr;
    }

    @NonNull
    private AlertDialog.Builder buildDialog(boolean[] hoursExcluded, boolean[] itemsChecked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.hours_absent_title))
                .setMultiChoiceItems(mItems, itemsChecked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index, boolean isChecked) {
                        int realIndex = Integer.parseInt(mItems[index].substring(0, 1))-1;
                        mHoursAbsent[realIndex] = isChecked;
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendResult();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Dialog closes
                    }
                });

        if (Utils.getAbsenceCount(hoursExcluded) == 0) {
            builder.setNeutralButton(R.string.hours_absent_all, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkAllAbsences();
                    sendResult();
                }
            });
        }
        return builder;
    }

    private void sendResult() {
        if (getTargetFragment() == null) return;
        Intent data = new Intent();
        data.putExtra(EXTRA_HOURS, mHoursAbsent);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
    }

    private void checkAllAbsences() {
        for (int i = 0; i < mHoursAbsent.length; i++) {
            mHoursAbsent[i] = true;
        }
    }
}
