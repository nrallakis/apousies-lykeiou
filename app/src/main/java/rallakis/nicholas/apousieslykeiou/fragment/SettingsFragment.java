package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;


import java.util.Locale;

import rallakis.nicholas.apousieslykeiou.helper.Logger;
import rallakis.nicholas.apousieslykeiou.model.AbsenceLab;
import rallakis.nicholas.apousieslykeiou.R;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final int REQUEST_CUSTOM_LIMITS = 15;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_main);
        setupResetPreference();
        setupPrivacyPolicyPreference();
        setupNotifyPreference();
        setupHoursPerDayPreference();
        setupSchoolTypePreference();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        Preference preference = findPreference(key);
        if (preference == null) return;
        if (preference instanceof CheckBoxPreference) {
            boolean isChecked = ((CheckBoxPreference) preference).isChecked();
            if (key.equals(getString(R.string.key_school_performance))) {
                ListPreference listPref = (ListPreference) findPreference(key);
                handleLimitSchoolTypeSelection(listPref);
            } else if (key.equals(getString(R.string.key_notify))) {
                String activated = getString(R.string.notifications_activated);
                String deactivated = getString(R.string.notifications_deactivated);
                preference.setSummary(isChecked ? activated : deactivated);
            }
            AbsenceLab.get(getContext()).updateMaxValues();
        }
        if (preference instanceof ListPreference) {
            ListPreference listPref = (ListPreference) preference;
            if (key.equals(getString(R.string.key_school_type))) {
                handleLimitSchoolTypeSelection(listPref);
            }
        }
    }

    private void setupHoursPerDayPreference() {
        ListPreference hoursPerDay = (ListPreference) findPreference(getString(R.string.key_hours_day));
        hoursPerDay.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                openHoursChangeWarningDialogWithResult((ListPreference) preference);
                return true;
            }
        });
        hoursPerDay.setSummary(hoursPerDay.getValue() + " ώρες");
    }

    private void setupNotifyPreference() {
        CheckBoxPreference notify = (CheckBoxPreference) findPreference(getString(R.string.key_notify));
        notify.setSummary(notify.isChecked() ? "Ενεργοποιημένες" : "Απενεργοποιημένες");
    }

    private void setupPrivacyPolicyPreference() {
        Preference privacyPolicyPreference = findPreference(getString(R.string.key_privacy_policy));
        privacyPolicyPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String url = getString(R.string.privacy_policy_link);
                Intent openPage = new Intent(Intent.ACTION_VIEW);
                openPage.setData(Uri.parse(url));
                startActivity(openPage);
                return true;
            }
        });
    }

    private void setupResetPreference() {
        Preference resetPreference = findPreference(getString(R.string.key_reset));
        resetPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Είσαι σίγουρος/η;");
                builder.setNegativeButton(R.string.cancel, null);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AbsenceLab.get(getActivity()).resetAbsences();
                        Logger.logResetAbsences();
                    }
                }).setMessage(R.string.reset_warning);
                builder.create().show();
                return true;
            }
        });
    }

    private void setupSchoolTypePreference() {
        ListPreference schoolType = (ListPreference) findPreference(getString(R.string.key_school_type));
        schoolType.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.equals(getString(R.string.custom_limit))) {
                    showCustomLimitDialog();
                }
                return true;
            }
        });

        AbsenceLab lab = AbsenceLab.get(getContext());
        setSchoolTypeSummary(schoolType, lab.getMaxAbsences());

        SharedPreferences prefs = getPreferenceScreen().getSharedPreferences();
        String savedSchoolType = prefs.getString(getString(R.string.key_school_type), getString(R.string.geniko));
        if (savedSchoolType.equals(getString(R.string.other)) || savedSchoolType.equals(getString(R.string.custom_limit))) {
            schoolType.setValue(getString(R.string.custom_limit));
        } else {
            schoolType.setValue(savedSchoolType);
        }
    }

    private void handleLimitSchoolTypeSelection(ListPreference listPref) {
        int total = 0;

        int selectedIndex = listPref.findIndexOfValue(listPref.getValue());
        switch (selectedIndex) {
            case 0: // Γενικό
            case 3: // ΕΠΑΛ
                total = 114;
                break;
            case 1:
            case 2:  // Εσπερινό, Μουσικό
                total = 130;
                break;
            case 4:  //Επαλ Ημερ (Ειδικότητες)
                total = 75;
                break;
            case 5:  //Επαλ Εσπερ Β-Γ (Ειδικότητες)
                total = 78;
                break;
            case 6:  //Επαλ Εσπερ Δ (Ειδικότητες)
                total = 83;
                break;
        }

        setSchoolTypeSummary(listPref, total);
        saveMaxValuesToFile(total);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_CUSTOM_LIMITS) {
            // Called when a custom limit has been set
            Preference schoolType = findPreference(getString(R.string.key_school_type));
            int justifiedLimit = data.getIntExtra(CustomLimitDialogFragment.EXTRA_TOTAL_LIMIT, 64);
            setSchoolTypeSummary(schoolType, justifiedLimit);

            saveMaxValuesToFile(justifiedLimit);
        }
    }

    private void saveMaxValuesToFile(int total) {
        SharedPreferences.Editor editor = getPreferenceScreen().getSharedPreferences().edit();
        editor.putInt(AbsenceLab.KEY_MAX_ABSENCES, total);
        editor.apply();

        AbsenceLab.get(getContext()).updateMaxValues();
        Logger.logLimitsSet(total);
    }

    private void showCustomLimitDialog() {
        CustomLimitDialogFragment fragment = new CustomLimitDialogFragment();
        fragment.setTargetFragment(this, REQUEST_CUSTOM_LIMITS);
        fragment.show(getFragmentManager(), CustomLimitDialogFragment.TAG);
    }

    private void openHoursChangeWarningDialogWithResult(final ListPreference preference) {
        final String previousOption = preference.getValue();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                preference.setValue(previousOption);
            }
        }).setPositiveButton(R.string.delete_caps, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AbsenceLab.get(getActivity()).resetAbsences();
                Logger.logResetAbsences();
                String value = preference.getValue();
                preference.setSummary(value + " ώρες");
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                preference.setValue(previousOption);
            }
        }).setMessage(getString(R.string.change_hours_day_message))
            .create().show();
    }

    private void setSchoolTypeSummary(Preference preference, int total) {
        preference.setSummary(String.format(new Locale("el"), "Συνολικά: %d ", total));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}
