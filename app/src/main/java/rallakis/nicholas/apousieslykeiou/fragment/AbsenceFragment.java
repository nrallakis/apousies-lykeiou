package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.NavUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.activity.AbsenceHomeActivity;
import rallakis.nicholas.apousieslykeiou.helper.Utils;
import rallakis.nicholas.apousieslykeiou.model.AbsenceLab;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;

import static rallakis.nicholas.apousieslykeiou.helper.Utils.formatHours;

public class AbsenceFragment extends Fragment implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener {

    public static final String TAG = "fragment_absence";

    public static final String EXTRA_DATE = "rallakis.nicholas.apousieslykeiou.date";
    public static final String EXTRA_ABSENCES = "rallakis.nicholas.apousieslykeiou.absences";
    public static final String EXTRA_REASON = "rallakis.nicholas.apousieslykeiou.reason";
    public static final String EXTRA_HOURS_ABSENT = "rallakis.nicholas.apousieslykeiou.hoursabsent";

    private static final int REQUEST_HOURS = 1;
    private static final int REQUEST_REASON = 2;

    private boolean[] mSelectedHours;
    private boolean[] mExcludedHours;

    private Date mSelectedDate;

    private TextView mDateView;
    private TextView mHoursAbsentView;
    private TextView mReasonView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.absence_title));

        int hoursPerDay = AbsenceLab.get(getActivity()).getHoursPerDay();
        mSelectedHours = new boolean[hoursPerDay];
        mExcludedHours = new boolean[hoursPerDay];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_absence, container, false);
        initViews(view);
        initListeners(view);
        return view;
    }

    private void initViews(View view) {
        mDateView = view.findViewById(R.id.tv_date);
        mHoursAbsentView = view.findViewById(R.id.tv_hours_absent);
        mReasonView = view.findViewById(R.id.tv_reason);
    }

    private void initListeners(View view) {
        view.findViewById(R.id.layout_date).setOnClickListener(this);
        view.findViewById(R.id.layout_absences).setOnClickListener(this);
        view.findViewById(R.id.layout_reason).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.layout_date:
                showDatePickerDialog();
                break;
            case R.id.layout_absences:
                showAbsencePickerDialog();
                break;
            case R.id.layout_reason:
                showReasonPickerDialog();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_absence, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_done:
                handleInput();
                break;
            case android.R.id.home:
                NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), AbsenceHomeActivity.class));
                getActivity().finish();
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

        return super.onOptionsItemSelected(item);
    }

    public void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        datePickerDialog.show();
    }

    public void showAbsencePickerDialog() {
        if (mSelectedDate == null) {
            Toast.makeText(getActivity(), "Δεν έχεις επιλέξει ημερομηνία!", Toast.LENGTH_SHORT).show();
            return;
        }

        AbsenceHoursDialogFragment fragment = AbsenceHoursDialogFragment.newInstance(mSelectedHours, mExcludedHours);
        fragment.setTargetFragment(this, REQUEST_HOURS);
        fragment.show(getFragmentManager(), AbsenceHoursDialogFragment.TAG);
    }

    public void showReasonPickerDialog() {
        AbsenceReasonPickerDialog fragment = AbsenceReasonPickerDialog.newInstance();
        fragment.setTargetFragment(this, REQUEST_REASON);
        fragment.show(getFragmentManager(), AbsenceReasonPickerDialog.TAG);
    }

    private void handleInput() {
        if (mSelectedDate == null) {
            Toast.makeText(getActivity(), "Δεν έχεις επιλέξει ημερομηνία!", Toast.LENGTH_SHORT).show();
            return;
        }

        int absences = Utils.getAbsenceCount(mSelectedHours);
        if (absences == 0) {
            Toast.makeText(getActivity(), "Δεν έχεις επιλέξει ώρες!", Toast.LENGTH_SHORT).show();
            return;
        }

        String reason = mReasonView.getText().toString();
        sendResultsToHomeFragment(mSelectedDate, absences, reason, mSelectedHours);
    }

    private void sendResultsToHomeFragment(Date date, int absences, String reason, boolean[] hoursAbsent) {
        Intent i = new Intent();
        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        i.putExtra(EXTRA_DATE, date);
        i.putExtra(EXTRA_ABSENCES, absences);
        i.putExtra(EXTRA_REASON, reason);
        i.putExtra(EXTRA_HOURS_ABSENT, hoursAbsent);

        // Send the results back to AbsenceHomeFragment
        getActivity().setResult(Activity.RESULT_OK, i);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case REQUEST_HOURS:
                mSelectedHours = data.getBooleanArrayExtra(AbsenceHoursDialogFragment.EXTRA_HOURS);
                updateHoursAbsentView();
                break;
            case REQUEST_REASON:
                String selectedReason = data.getStringExtra(EXTRA_REASON);
                mReasonView.setText(selectedReason);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        mSelectedDate = new GregorianCalendar(year, month, day).getTime();
        updateDateView();

        AbsenceLab lab = AbsenceLab.get(getActivity().getApplicationContext());
        ArrayList<boolean[]> listOfHoursExcludedFromSameDay = new ArrayList<>();
        for (AbsenceObject object : lab.getAbsenceObjectsOnDate(mSelectedDate)) {
            listOfHoursExcludedFromSameDay.add(object.getHoursAbsent());
        }
        mExcludedHours = Utils.getHoursExcluded(listOfHoursExcludedFromSameDay, getHoursPerDay());

        // The user may have already selected hours so reset them.
        resetHoursAbsentView();
    }

    private int getHoursPerDay() {
        return AbsenceLab.get(getActivity()).getHoursPerDay();
    }

    private void resetHoursAbsentView() {
        mHoursAbsentView.setText(null);
        clearSelectedHours();
    }

    private void clearSelectedHours() {
        for (int i = 0; i < mSelectedHours.length; i++) {
            mSelectedHours[i] = false;
        }
    }

    private void updateDateView() {
        mDateView.setText(Utils.formatDate(mSelectedDate));
    }

    private void updateHoursAbsentView() {
        if (Utils.getAbsenceCount(mSelectedHours) == 0) {
            mHoursAbsentView.setText(null); // Set the default text hint
        } else {
            mHoursAbsentView.setText(formatHours(mSelectedHours));
        }
    }
}
