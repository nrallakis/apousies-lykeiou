package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.NavUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import rallakis.nicholas.apousieslykeiou.model.AbsenceAdapter;
import rallakis.nicholas.apousieslykeiou.model.AbsenceLab;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;
import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.activity.AbsenceHomeActivity;
import rallakis.nicholas.apousieslykeiou.helper.Utils;

public class AbsenceHistoryFragment extends ListFragment implements AbsListView.MultiChoiceModeListener, AdapterView.OnItemSelectedListener {

    private static final String KEY_SHOW = "key_show";

    private static final int REQUEST_JUSTIFICATION = 10;
    public static final String EXTRA_WAS_JUSTIFIED = "absence.was.justified";

    private ArrayList<AbsenceObject> mAbsenceObjects;
    private ArrayList<AbsenceObject> mSelected;

    private AbsenceAdapter mAdapter;

    private int mSelectedSorting;

    public static AbsenceHistoryFragment newInstance() {
        return new AbsenceHistoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mAbsenceObjects = AbsenceLab.get(getActivity()).getAbsenceObjects();
        mSelected = new ArrayList<>();
        mAdapter = new AbsenceAdapter(getActivity(), mAbsenceObjects);

        Utils.sortByDate(mAbsenceObjects);

        setListAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AbsenceAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Add a custom view to show when the list is empty.
        View emptyView = getActivity().getLayoutInflater().inflate(R.layout.absence_empty_view, null);
        ((ViewGroup) getListView().getParent()).addView(emptyView);
        getListView().setEmptyView(emptyView);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        AbsenceObject absenceObject = (AbsenceObject) getListAdapter().getItem(position);
        AbsenceDetailsDialogFragment dialog = AbsenceDetailsDialogFragment.newInstance(absenceObject);
        dialog.setTargetFragment(this, REQUEST_JUSTIFICATION);
        dialog.show(getFragmentManager(), AbsenceDetailsDialogFragment.TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ListView listView = view.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_history, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sort) {
            openSortDialog();
            return true;
        } else if (id == R.id.action_reverse) {
            mAdapter.reverseOrder();
            mAdapter.notifyDataSetChanged();
            return true;
        } else if (id == android.R.id.home) {
            NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), AbsenceHomeActivity.class));
            getActivity().finish();
            getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        return false;
    }

    private void openSortDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Ταξινόμηση");
        builder.setSingleChoiceItems(R.array.filter_items, mSelectedSorting, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                mSelectedSorting = position;
                switch (position) {
                    case 0:
                        Utils.sortByDate(mAbsenceObjects);
                        mAdapter.notifyDataSetChanged();
                        break;

                    case 1:
                        Utils.sortByAbsenceCount(mAbsenceObjects);
                        mAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });

        builder.create().show();
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
        if (checked) {
            mSelected.add(mAdapter.getItem(position));
        } else {
            mSelected.remove(mAdapter.getItem(position));
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.menu_action_mode, menu);
        actionMode.setTitle("Διαγραφή");
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.action_delete:
                AbsenceLab lab = AbsenceLab.get(getActivity());
                for (AbsenceObject object : mSelected) {
                    lab.deleteAbsenceObject(object);
                }
                actionMode.finish();
                mAdapter.notifyDataSetChanged();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {}

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        // Called when a spinner item has been selected
        switch (position) {
            case 0:
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // Do nothing.
    }
}
