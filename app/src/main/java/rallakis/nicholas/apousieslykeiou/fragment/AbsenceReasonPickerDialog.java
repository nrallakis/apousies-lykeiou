package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import rallakis.nicholas.apousieslykeiou.R;

import static rallakis.nicholas.apousieslykeiou.fragment.AbsenceFragment.EXTRA_REASON;

public class AbsenceReasonPickerDialog extends DialogFragment {

    public static final String TAG = "absence_reason_picker_dialog";

    public AbsenceReasonPickerDialog() {}

    public static AbsenceReasonPickerDialog newInstance() {
        return new AbsenceReasonPickerDialog();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Διάλεγε λόγο απουσίας")
                .setItems(R.array.reason_options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {
                        if (getTargetFragment() == null) return;

                        String selectedReason = getResources()
                                .getStringArray(R.array.reason_options)[index];

                        Intent data = new Intent();
                        data.putExtra(EXTRA_REASON, selectedReason);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
                    }
                });

        return builder.create();
    }
}
