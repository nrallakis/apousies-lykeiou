package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.preference.PreferenceManager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.Date;

import rallakis.nicholas.apousieslykeiou.AdManager;
import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.activity.AbsenceActivity;
import rallakis.nicholas.apousieslykeiou.activity.AbsenceHistoryActivity;
import rallakis.nicholas.apousieslykeiou.activity.SettingsActivity;
import rallakis.nicholas.apousieslykeiou.helper.Logger;
import rallakis.nicholas.apousieslykeiou.helper.Utils;
import rallakis.nicholas.apousieslykeiou.model.AbsenceLab;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;


public class AbsenceHomeFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = AbsenceHomeFragment.class.getSimpleName();

    private static final int REQUEST_ABSENCEDAY = 0;

    private static final long ANIMATION_SPEED = 3000 / 114;

    private TextView mTotalLabel;
    private TextView mTotalAbsencesLabel;
    private ProgressBar mProgress;
    private FloatingActionButton mFab;

    private AbsenceLab mAbsenceLab;

    @Override
    public void onPause() {
        super.onPause();
        mAbsenceLab.saveAbsenceDays();
    }

    @Override
    public void onStart() {
        super.onStart();
        AdManager adManager = AdManager.getInstance(getActivity());
        if (adManager.shouldShowAd()) {
            adManager.showAds();
        }
        updateUI();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        initListeners(view);
        updateUI();
        actIfFirstLaunch();
        return view;
    }

    private void initViews(View view) {
        mTotalLabel = view.findViewById(R.id.tv_total);
        mTotalAbsencesLabel = view.findViewById(R.id.tv_total_absences);
        mProgress = view.findViewById(R.id.progress);
        mFab = view.findViewById(R.id.fab);
    }

    private void initListeners(View view) {
        mFab.setOnClickListener(this);
        view.findViewById(R.id.circle_progress_layout).setOnClickListener(this);
        view.findViewById(R.id.layout_total_absences).setOnClickListener(this);
    }

    private void actIfFirstLaunch() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        boolean firstLaunchUpdate = prefs.getBoolean(getString(R.string.key_first_launch_update), true);
        if (firstLaunchUpdate) {
            String firstLaunchUpdateKey = getString(R.string.key_first_launch_update);
            prefs.edit().putBoolean(firstLaunchUpdateKey, false).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mProgress.setMax(mAbsenceLab.getMaxAbsences());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAbsenceLab = AbsenceLab.get(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                Logger.logSettingsClicked();
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                return true;
            case R.id.action_history:
                Logger.logHistoryActionClicked();
                openAbsenceHistory();
                return true;
            case R.id.action_rate:
                Logger.logRateButtonClicked();
                Utils.rateApp(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Get the result data from the new absentDay created.
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_ABSENCEDAY) {
            AdManager.getInstance(getActivity()).incrementMoves();
            handleRequestMakeAbsenceDay(data);
        }
    }

    private void updateUI() {
        int totalAbsences = mAbsenceLab.getTotalAbsences();
        mTotalLabel.setText(String.valueOf(totalAbsences));
        mTotalAbsencesLabel.setText(formatTotalAbsences(totalAbsences, mAbsenceLab.getMaxAbsences()));
        mProgress.setProgress(totalAbsences);
    }

    private void handleRequestMakeAbsenceDay(Intent data) {
        Date date = (Date) data.getSerializableExtra(AbsenceFragment.EXTRA_DATE);
        int absenceCount = data.getIntExtra(AbsenceFragment.EXTRA_ABSENCES, 1);
        String reason = data.getStringExtra(AbsenceFragment.EXTRA_REASON);
        boolean[] hoursAbsent = data.getBooleanArrayExtra(AbsenceFragment.EXTRA_HOURS_ABSENT);

        AbsenceObject object = new AbsenceObject(absenceCount, reason, date, hoursAbsent);
        mAbsenceLab.addAbsenceObject(object);

        Logger.logAbsenceCreation(object);

        updateUI();
    }

    @Override
    public void onClick(View view) {
        //Handle clicks
        int id = view.getId();
        switch (id) {
            case R.id.fab:
                openAbsenceFragment();
                break;
            case R.id.circle_progress_layout:
                Logger.logProgressCircleClick();
                openAbsenceHistory();
                break;
            case R.id.layout_total_absences:
                Logger.logAllAbsencesViewClicked();
                openAbsenceHistory();
                break;
        }
    }

    private void openAbsenceHistory() {
        AdManager.getInstance(getActivity()).incrementMoves();
        Intent intent = new Intent(getActivity(), AbsenceHistoryActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void openAbsenceFragment() {
        Intent intent = new Intent(getActivity(), AbsenceActivity.class);
        startActivityForResult(intent, REQUEST_ABSENCEDAY);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private Spanned formatTotalAbsences(int total, int maxTotal) {
        return Html.fromHtml(getString(R.string.total_label, String.valueOf(total), String.valueOf(maxTotal)));
    }
}
