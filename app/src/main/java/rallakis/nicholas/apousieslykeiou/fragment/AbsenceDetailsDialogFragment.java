package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.helper.Utils;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;


public class AbsenceDetailsDialogFragment extends DialogFragment {

    public static final String TAG = "absence_details_dialog";

    private static final String KEY_HOURS = "hours";
    private static final String KEY_REASON = "reason";
    private static final String KEY_DATE = "date";
    private static final String KEY_ID = "id";

    public AbsenceDetailsDialogFragment() {}

    public static AbsenceDetailsDialogFragment newInstance(AbsenceObject object) {
        Bundle args = new Bundle();
        args.putBooleanArray(KEY_HOURS, object.getHoursAbsent());
        args.putString(KEY_REASON, object.getReason());
        args.putSerializable(KEY_ID, object.getId());
        args.putLong(KEY_DATE, object.getDate().getTime());

        AbsenceDetailsDialogFragment fragment = new AbsenceDetailsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean[] hoursAbsent = getArguments().getBooleanArray(KEY_HOURS);
        String reason = getArguments().getString(KEY_REASON);
        final Date date = new Date(getArguments().getLong(KEY_DATE));

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy", new Locale("el"));
        String formattedDate = format.format(date);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.details_title))
                .setMessage(formatMessage(formattedDate, hoursAbsent, reason))
                .setPositiveButton(R.string.ok, null);

        return builder.create();
    }

    private String formatMessage(String formattedDate, boolean hoursAbsent[], String reason) {
        return String.format("\n Στις %s \n\n %s \n\n %s", formattedDate, Utils.formatHours(hoursAbsent), reason);
    }
}
