package rallakis.nicholas.apousieslykeiou.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import rallakis.nicholas.apousieslykeiou.R;

public class CustomLimitDialogFragment extends DialogFragment {

    static final String TAG = "custom_limit_dialog";

    static final String EXTRA_TOTAL_LIMIT = "rallakis.nicholas.apousieslykeioy.totallimit";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.custom_limit_dialog, null);

        final EditText etTotal = dialogView.findViewById(R.id.custom_total);

        builder.setTitle("Προσαρμογή ορίου");
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (getTargetFragment() == null) return;

                if (TextUtils.isEmpty(etTotal.getText())) {
                    Toast.makeText(getContext(), "Δεν έχεις ορίσει τιμές", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent data = new Intent();
                try {
                    data.putExtra(EXTRA_TOTAL_LIMIT, Integer.parseInt(etTotal.getText().toString()));
                } catch (NumberFormatException e) {
                    Toast.makeText(getContext(), "Δεν όρισες το πλήθος των απουσιών", Toast.LENGTH_SHORT).show();
                }

                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
            }
        });

        return builder.create();
    }
}
