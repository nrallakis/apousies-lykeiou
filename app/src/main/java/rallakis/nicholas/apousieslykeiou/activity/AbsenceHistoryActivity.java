package rallakis.nicholas.apousieslykeiou.activity;

import android.app.FragmentManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.fragment.AbsenceHistoryFragment;

public class AbsenceHistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupBanner();

        FragmentManager fm = getFragmentManager();
        AbsenceHistoryFragment fragment = (AbsenceHistoryFragment) fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = AbsenceHistoryFragment.newInstance();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setTitle(R.string.all_absences);
    }

    private void setupBanner() {
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
