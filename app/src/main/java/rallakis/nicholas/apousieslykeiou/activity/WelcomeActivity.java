package rallakis.nicholas.apousieslykeiou.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.widget.SwitchCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.helper.EmptyInputException;
import rallakis.nicholas.apousieslykeiou.model.AbsenceLab;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;

public class WelcomeActivity extends AppCompatActivity {

    private SharedPreferences mPrefs;

    private ViewPager mViewPager;
    private LinearLayout mDotsLayout;
    private Button mButtonNext;

    private int[] mSlideLayouts;
    private int maxAbsences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!isFirstLaunch()) {
            launchHomeScreen();
            finish();
        }

        makeNotificationBarTransparent();
        setContentView(R.layout.activity_welcome);

        mViewPager = findViewById(R.id.view_pager);
        mDotsLayout = findViewById(R.id.layout_dots);
        mButtonNext = findViewById(R.id.btn_next);
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonNextClicked();
            }
        });

        mSlideLayouts = new int[]{ R.layout.welcome_slide1, R.layout.welcome_slide2, R.layout.welcome_slide3 };
        addSlidersBottomDots(0);
        initViewPager();
    }

    private void initViewPager() {
        SlidersViewPagerAdapter adapter = new SlidersViewPagerAdapter();
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                addSlidersBottomDots(position);

                // Change button label on last slider
                if (position == mSlideLayouts.length-1) {
                    mButtonNext.setText(R.string.end);
                } else {
                    mButtonNext.setText(R.string.next);
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }


    public void onButtonNextClicked() {
        int current = getItem(1);
        if (current < mSlideLayouts.length) {
            mViewPager.setCurrentItem(current);
        } else {
            onLastSlider();
        }
    }

    private void onLastSlider() {
        Spinner schoolTypeSpinner = findViewById(R.id.sp_school);
        String schoolType = schoolTypeSpinner.getSelectedItem().toString();

        try {
            if (schoolType.equals(getString(R.string.other))) { //Άλλο
                handleSchoolTypeOther();
            } else {
                maxAbsences = getTotalAbsencesForSchoolType(schoolType);
            }

            SwitchCompat beenAbsentSwitch = findViewById(R.id.sc_absent);
            if (beenAbsentSwitch.isChecked()) {
                handleBeenAbsent();
            }
        } catch (EmptyInputException e) {
            return;
        }

        String hoursPerDay = ((Spinner) findViewById(R.id.sp_hours)).getSelectedItem().toString();
        saveOptionsToFile(hoursPerDay, maxAbsences, schoolType);
        launchHomeScreen();
    }

    private void handleSchoolTypeOther() throws EmptyInputException {
        EditText etTotal = findViewById(R.id.custom_total);
        etTotal.setTransformationMethod(null);

        if (TextUtils.isEmpty(etTotal.getText())) {
            showNoNumbersToast();
            throw new EmptyInputException();
        }

        maxAbsences = Integer.parseInt(etTotal.getText().toString());
    }

    private void handleBeenAbsent() throws EmptyInputException {
        EditText etAbsence = findViewById(R.id.absences_start);
        String text = etAbsence.getText().toString();
        if (TextUtils.isEmpty(text)) {
            showNoNumbersToast();
            throw new EmptyInputException();
        }

        int absenceCount = Integer.parseInt(text);
        String reason = "Προηγούμενες απουσίες";
        Date date = new Date();
        boolean[] hoursAbsent = new boolean[]{};
        AbsenceObject absenceObject = new AbsenceObject(absenceCount, reason, date, hoursAbsent);
        AbsenceLab.get(this).addAbsenceObject(absenceObject);
    }

    private void saveOptionsToFile(String hoursPerDay, int maxAbsences, String schoolType) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putString(getString(R.string.key_hours_day), hoursPerDay);
        editor.putInt(AbsenceLab.KEY_MAX_ABSENCES, maxAbsences);
        editor.putString(getString(R.string.key_school_type), schoolType);
        editor.apply();

        AbsenceLab.get(getApplicationContext()).updateMaxValues();
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private void addSlidersBottomDots(int currentPage) {
        TextView[] dots = new TextView[mSlideLayouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        mDotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            mDotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[currentPage].setTextColor(colorsActive[currentPage]);
        }
    }

    private boolean isFirstLaunch() {
        return mPrefs.getBoolean(getString(R.string.key_first_launch), true);
    }

    private void launchHomeScreen() {
        mPrefs.edit().putBoolean(getString(R.string.key_first_launch), false).apply();
        Intent homeActivity = new Intent(this, AbsenceHomeActivity.class);
        startActivity(homeActivity);
        finish();
    }

    private void makeNotificationBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private int getTotalAbsencesForSchoolType(String schoolType) {
        if (schoolType.equals(getString(R.string.geniko)) || schoolType.equals(getString(R.string.epal))) { // Γενικό
            return 114;
        } else if (schoolType.equals(getString(R.string.mousiko)) || schoolType.equals(getString(R.string.esperino))) { // Εσπερινό, Μουσικό
            return 130;
        } else if (schoolType.equals(getString(R.string.epal_hmer))) {
            return 75;
        } else if (schoolType.equals(getString(R.string.epal_esper_bc))) {
            return 78;
        } else if (schoolType.equals(getString(R.string.epal_esper_d))) {
            return 83;
        }
        return 0;
    }


    private class SlidersViewPagerAdapter extends PagerAdapter {

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = mInflater.inflate(mSlideLayouts[position], container, false);
            container.addView(view);

            if (position != mSlideLayouts.length-1) { // Not on last slide
                return view;
            }

            // On last slide
            Spinner schoolType = findViewById(R.id.sp_school);
            ArrayAdapter adapter = ArrayAdapter.createFromResource(WelcomeActivity.this, R.array.welcome_school_entries, R.layout.spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            schoolType.setAdapter(adapter);
            schoolType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    boolean selectedOther = position == 7;
                    findViewById(R.id.layout_custom_limit)
                            .setVisibility(selectedOther ? View.VISIBLE : View.GONE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {}
            });

            Spinner hoursPerDay = findViewById(R.id.sp_hours);

            ArrayAdapter hoursAdapter = ArrayAdapter.createFromResource(WelcomeActivity.this, R.array.hours_per_day_values, R.layout.spinner_item);
            hoursAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            hoursPerDay.setAdapter(hoursAdapter);
            hoursPerDay.setSelection(4);

            SwitchCompat absentSwitch = findViewById(R.id.sc_absent);
            absentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    View v = findViewById(R.id.layout_absences_start);
                    v.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                }
            });

            return view;
        }

        @Override
        public int getCount() {
            return mSlideLayouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void showNoNumbersToast() {
        Toast.makeText(WelcomeActivity.this, "Βάλε νούμερα", Toast.LENGTH_SHORT).show();
    }
}
