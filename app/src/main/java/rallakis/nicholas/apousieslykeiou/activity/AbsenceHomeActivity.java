package rallakis.nicholas.apousieslykeiou.activity;

import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.MobileAds;

import rallakis.nicholas.apousieslykeiou.fragment.AbsenceHomeFragment;

public class AbsenceHomeActivity extends AbsenceAbstractActivity {

    @Override
    protected void beforeInit() {
        MobileAds.initialize(this, "ca-app-pub-7739991390722327~8095907987");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected Fragment createFragment() {
        return new AbsenceHomeFragment();
    }
}
