package rallakis.nicholas.apousieslykeiou.activity;

import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;

import rallakis.nicholas.apousieslykeiou.fragment.AbsenceFragment;

public class AbsenceActivity extends AbsenceAbstractActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected Fragment createFragment() {
        return new AbsenceFragment();
    }
}
