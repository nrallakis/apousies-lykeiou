package rallakis.nicholas.apousieslykeiou.model;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rallakis.nicholas.apousieslykeiou.R;

import static rallakis.nicholas.apousieslykeiou.R.id.date;
import static rallakis.nicholas.apousieslykeiou.helper.Utils.formatDate;

public class AbsenceAdapter extends ArrayAdapter<AbsenceObject> {

    private ArrayList<AbsenceObject> mObjects;

    public AbsenceAdapter(Context context, List<AbsenceObject> absenceObjects) {
        super(context, 0, absenceObjects);
        mObjects = (ArrayList<AbsenceObject>) absenceObjects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext())
                    .inflate(R.layout.absence_list_item, parent, false);
        }

        //Find the absence object at the given position
        AbsenceObject absenceObject = getItem(position);

        TextView absencesView = listItemView.findViewById(R.id.absences);
        absencesView.setText(String.valueOf(absenceObject.getAbsenceCount()));

        applyIconColor(absencesView);

        TextView reasonView = listItemView.findViewById(R.id.reason);
        reasonView.setText(absenceObject.getReason());

        TextView dateView = listItemView.findViewById(date);
        dateView.setText(formatDate(absenceObject.getDate()));

        return listItemView;
    }

    private void applyIconColor(View view) {
        GradientDrawable absencesCircle = (GradientDrawable) view.getBackground();
        int color = ContextCompat.getColor(getContext(), R.color.colorAccent);
        absencesCircle.setColor(color);
    }

    @Nullable
    @Override
    public AbsenceObject getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(@Nullable AbsenceObject item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    public void reverseOrder() {
        Collections.reverse(mObjects);
    }
}
