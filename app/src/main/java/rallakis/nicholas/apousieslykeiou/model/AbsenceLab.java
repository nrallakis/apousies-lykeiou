package rallakis.nicholas.apousieslykeiou.model;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.helper.AbsenceJSONSerializer;

public class AbsenceLab {

    private static final String TAG = "AbsenceLab";
    private static final String FILENAME = "absences.json";

    public static final String KEY_MAX_ABSENCES = "max_absences";

    private static final int DEFAULT_MAX_ABSENCES = 114;
    private static volatile AbsenceLab instance;

    public static final int MAX_ABSENCES_PER_DAY = 10;

    private static int MAX_ABSENCES;
    
    private Context mAppContext;
    
    private ArrayList<AbsenceObject> mAbsenceObjects;
    private AbsenceJSONSerializer mSerializer;

    public static AbsenceLab get(Context c) {
        if (instance == null) { //Double checked locking
            synchronized (AbsenceLab.class) {
                if (instance == null) {
                    instance = new AbsenceLab(c.getApplicationContext());
                }
            }
        }
        return instance;
    }

    private AbsenceLab(Context appContext) {
        mAppContext = appContext;
        mAbsenceObjects = new ArrayList<>();
        mSerializer = new AbsenceJSONSerializer(mAppContext, FILENAME);
        try {
            mAbsenceObjects = mSerializer.loadAbsenceObjects();
        } catch (Exception e) {
            mAbsenceObjects = new ArrayList<>();
            Log.e(TAG, "Error loading absenceDays ", e);
        }

        updateMaxValues();
    }

    /** Slow method, use it only when necessary */
    public void updateMaxValues() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mAppContext);
        MAX_ABSENCES = prefs.getInt(KEY_MAX_ABSENCES, DEFAULT_MAX_ABSENCES);
    }

    public boolean saveAbsenceDays() {
        try {
            mSerializer.saveAbsenceDays(mAbsenceObjects);
            Log.d(TAG, "Absences saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving absences");
            return false;
        }
    }

    public ArrayList<AbsenceObject> getAbsenceObjects() {
        return mAbsenceObjects;
    }

    public AbsenceObject getAbsence(UUID id) {
        for (AbsenceObject object : mAbsenceObjects) {
            if (object.getId().equals(id)) return object;
        }
        return null;
    }

    public int getMaxAbsences() {
        return MAX_ABSENCES;
    }

    public int getTotalAbsences() {
        int count = 0;
        for (AbsenceObject day : mAbsenceObjects) {
            count += day.getAbsenceCount();
        }
        return count;
    }

    public void addAbsenceObject(AbsenceObject object) {
        mAbsenceObjects.add(0, object);
    }

    public void deleteAbsenceObject(AbsenceObject object) { mAbsenceObjects.remove(object); }

    public List<AbsenceObject> getAbsenceObjectsOnDate(Date selectedDate) {
        ArrayList<AbsenceObject> result = new ArrayList<>();
        for (AbsenceObject day : mAbsenceObjects) {
            if (day.getDate().equals(selectedDate)) {
                result.add(day);
            }
        }
        return result;
    }

    public void resetAbsences() {
        mAbsenceObjects.clear();
    }

    public int getHoursPerDay() {
        String key = mAppContext.getString(R.string.key_hours_day);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mAppContext);
        String value = prefs.getString(key, "7");
        return Integer.parseInt(value);
    }
}
