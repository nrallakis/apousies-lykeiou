package rallakis.nicholas.apousieslykeiou.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.UUID;

public class AbsenceObject {

    private static final String JSON_ABSENCES = "absences";
    private static final String JSON_REASON = "reason";
    private static final String JSON_DATE = "date";
    private static final String JSON_HOURS = "hours";
    private static final String JSON_ID = "id";

    public static final String EXTRA_ID = "rallakis.nicholas.apousieslykeioy.id";

    private int mAbsenceCount;
    private String mReason;
    private Date mDate;
    private boolean[] mHoursAbsent;
    private UUID mId;

    public AbsenceObject(int absenceCount, String reason, Date date, boolean[] hoursAbsent) {
        mAbsenceCount = absenceCount;
        mReason = reason;
        mDate = date;
        mHoursAbsent = hoursAbsent;
        mId = UUID.randomUUID();
    }

    public AbsenceObject(JSONObject json) throws JSONException {
        mAbsenceCount = json.getInt(JSON_ABSENCES);
        mReason = json.getString(JSON_REASON);
        mDate = new Date(json.getLong(JSON_DATE));
        mId = UUID.fromString(json.getString(JSON_ID));

        mHoursAbsent = new boolean[AbsenceLab.MAX_ABSENCES_PER_DAY];
        JSONArray hoursAbsent = json.getJSONArray(JSON_HOURS);
        for (int i = 0; i < hoursAbsent.length(); i++) {
            mHoursAbsent[i] = hoursAbsent.getBoolean(i);
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ABSENCES, mAbsenceCount);
        json.put(JSON_REASON, mReason);
        json.put(JSON_DATE, mDate.getTime());
        json.put(JSON_ID, mId.toString());

        JSONArray hours = new JSONArray();
        for (boolean wasAbsent : mHoursAbsent) {
            hours.put(wasAbsent);
        }
        json.put(JSON_HOURS, hours);

        return json;
    }

    public boolean[] getHoursAbsent() {
        return mHoursAbsent;
    }

    public int getAbsenceCount() {
        return mAbsenceCount;
    }

    public Date getDate() {
        return mDate;
    }

    public void setReason(String reason) {
        mReason = reason;
    }

    public String getReason() {
        return mReason;
    }

    @Override
    public String toString() {
        return mDate.toString();
    }

    public UUID getId() {
        return mId;
    }
}
