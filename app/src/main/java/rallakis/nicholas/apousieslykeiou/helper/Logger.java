package rallakis.nicholas.apousieslykeiou.helper;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;

public class Logger {

    private static Context context;

    public static void init(Context c) {
        context = c;
    }

    public static void logResetAbsences() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ResetAbsences");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ResetAbsences");
        FirebaseAnalytics.getInstance(context).logEvent("reset_absences", bundle);
    }

    public static void logLimitsSet(int total) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "AbsenceLimit");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "AbsenceLimit");
        bundle.putInt("total", total);
        FirebaseAnalytics.getInstance(context).logEvent("absence_limits_set", bundle);
    }

    public static void logAbsenceCreation(AbsenceObject object) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "AbsenceObject");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "AbsenceObject");
        bundle.putInt("absenceCount", object.getAbsenceCount());
        bundle.putLong("date", object.getDate().getTime());
        bundle.putString("reason", object.getReason());
        FirebaseAnalytics.getInstance(context).logEvent("absence_creation", bundle);
    }

    public static void logAllAbsencesViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "AllAbsencesView");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "AllAbsencesView");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logHistoryActionClicked() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "HistoryAbsencesView");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "HistoryAbsencesView");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logProgressCircleClick() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ProgressCircleView");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ProgressCircleView");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logSettingsClicked() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Settings");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Settings");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logRateButtonClicked() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "Rate");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Rate");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
