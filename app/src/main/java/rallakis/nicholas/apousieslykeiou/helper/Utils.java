package rallakis.nicholas.apousieslykeiou.helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import rallakis.nicholas.apousieslykeiou.R;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;

public class Utils {

    public static void rateApp(Context context) {
        Intent rateIntent;
        try {
            rateIntent = rateIntentForUrl("market://details", context);
        } catch (ActivityNotFoundException e) {
            rateIntent = rateIntentForUrl("https://play.gooogle.com/store/apps/details", context);
        }
        context.startActivity(rateIntent);
    }

    private static Intent rateIntentForUrl(String url, Context c) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, c.getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    public static int getAbsenceCount(boolean[] hoursAbsent) {
        int count = 0;
        for (boolean wasAbsent : hoursAbsent) {
            if (wasAbsent) count++;
        }
        return count;
    }

    /**
     * Return the formatted date string (i.e. "Δευτέρα, 11 Αυγούστου") from a date object.
     */
    public static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMMM", new Locale("el"));
        return dateFormat.format(date);
    }

    /**
     * Return the formatted absences string (i.e. "1η, 2η, 7η ώρα") from a boolean array.
     */
    public static String formatHours(boolean[] hoursAbsent) {
        if (getAbsenceCount(hoursAbsent) == hoursAbsent.length) {
            return "Όλη μέρα";
        }
        if (getAbsenceCount(hoursAbsent) == 2 && hoursAbsent[0] && hoursAbsent[1]) {
            return "Πρώτες δύο ώρες";
        }
        if (getAbsenceCount(hoursAbsent) == 3 && hoursAbsent[0] && hoursAbsent[1] && hoursAbsent[2]) {
            return "Πρώτες τρεις ώρες";
        }
        if (getAbsenceCount(hoursAbsent) == 4 && hoursAbsent[0] && hoursAbsent[1] && hoursAbsent[2] && hoursAbsent[3]) {
            return "Πρώτες τέσσερις ώρες";
        }
        if (getAbsenceCount(hoursAbsent) == 1 && hoursAbsent[hoursAbsent.length-1]) {
            return "Τελευταία ώρα";
        }
        if (getAbsenceCount(hoursAbsent) == 2 && hoursAbsent[hoursAbsent.length-2] && hoursAbsent[hoursAbsent.length-1]) {
            return "Τελευταίες δύο ώρες";
        }
        if (getAbsenceCount(hoursAbsent) == 3 && hoursAbsent[hoursAbsent.length-3] && hoursAbsent[hoursAbsent.length-2] && hoursAbsent[hoursAbsent.length-1]) {
            return "Τελευταίες τρεις ώρες";
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < hoursAbsent.length; i++) {
            if (hoursAbsent[i]) {
                builder.append(i+1).append("η, ");
            }
        }
        if (builder.length() >= 2) {
            builder.deleteCharAt(builder.length()-2); // Remove the last comma
            builder.append("ώρα");
        }
        return builder.toString();
    }

    /**
     * Returns a boolean array with the hours excluded given a list of hours excluded.
     * It works by adding the arrays like the bitwise AND operation.
     */
    public static boolean[] getHoursExcluded(ArrayList<boolean[]> listOfHoursExcluded, int hoursPerDay) {
        boolean[] result = new boolean[hoursPerDay];
        for (boolean[] hoursExcluded : listOfHoursExcluded) {
            for (int i = 0; i < result.length; i++) {
                if (hoursExcluded[i]) result[i] = true;
            }
        }
        return result;
    }

    private static final AbsenceDateComparator dateComparator = new AbsenceDateComparator();
    private static final AbsenceCountComparator countComparator = new AbsenceCountComparator();

    private static class AbsenceCountComparator implements Comparator<AbsenceObject> {
        @Override
        public int compare(AbsenceObject a, AbsenceObject b) {
            if (a.getAbsenceCount() == b.getAbsenceCount()) return 0;
            return a.getAbsenceCount() < b.getAbsenceCount() ? 1 : -1;
        }
    }

    private static class AbsenceDateComparator implements Comparator<AbsenceObject> {
        @Override
        public int compare(AbsenceObject a, AbsenceObject b) {
            return compareDatesByDate(a.getDate(), b.getDate());
        }
    }

    /**
     * Compares two dates object by date and not time.
     */
    public static int compareDatesByDate(Date a, Date b) {
        return getZeroTimeDate(a).compareTo(getZeroTimeDate(b)) * (-1); // Multiply by -1 to reverse order.
    }

    /**
     * Date.compare, compares the dates on the milliseconds. This method helps when you
     * want to compare two date objects on the date and not the time
     */
    public static Date getZeroTimeDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static void sortByAbsenceCount(ArrayList<AbsenceObject> objects) {
        Collections.sort(objects, countComparator);
    }

    public static void sortByDate(ArrayList<AbsenceObject> objects) {
        Collections.sort(objects, dateComparator);
    }

}
