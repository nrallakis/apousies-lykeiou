package rallakis.nicholas.apousieslykeiou;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import androidx.core.app.NotificationCompat;

import java.util.Date;

import rallakis.nicholas.apousieslykeiou.activity.AbsenceHistoryActivity;
import rallakis.nicholas.apousieslykeiou.helper.Utils;

public class AlarmReceiver extends BroadcastReceiver {

    public static final String EXTRA_DATE = "alarm_receiver_date";

    @Override
    public void onReceive(Context context, Intent intent) {
        long dateInMillis = intent.getLongExtra(EXTRA_DATE, 0);
        Date date = new Date(dateInMillis);
        createNotification(context, "Έχεις άλλες 3 μέρες για να δικαιολογήσεις της απουσίες που έκανες: " + Utils.formatDate(date));
    }

    private void createNotification(Context context, String contentText) {
        Intent intent = new Intent(context, AbsenceHistoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 110,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(context, "absence_notification")
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(Color.parseColor("#f7be21"))
                .setLargeIcon(bitmap)
                .setContentTitle("Υπενθύμιση")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(contentText))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(110, notificationBuilder.build());
    }
}
