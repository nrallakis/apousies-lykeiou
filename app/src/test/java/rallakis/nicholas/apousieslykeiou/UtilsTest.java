package rallakis.nicholas.apousieslykeiou;

import android.content.Context;
import android.content.Intent;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import rallakis.nicholas.apousieslykeiou.helper.Utils;
import rallakis.nicholas.apousieslykeiou.model.AbsenceObject;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UtilsTest {

    @Test
    public void utils_get_hours_excluded() throws Exception {
        boolean[] a = {true, false, false, false, false, true, true};
        boolean[] b = {false, false, false, false, true, true, true};
        ArrayList<boolean[]> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        boolean[] expected = {true, false, false, false, true, true, true};
        assertArrayEquals(expected, Utils.getHoursExcluded(list, 7));
    }

    @Test
    public void utils_get_hours_excluded2() throws Exception {
        boolean[] a = {true, false, false, false, false, false, false};
        boolean[] b = {false, false, false, false, true, true, true};
        ArrayList<boolean[]> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        boolean[] expected = {true, false, false, false, true, true, true};
        assertArrayEquals(expected, Utils.getHoursExcluded(list, 7));
    }

    @Test
    public void format_hours_all_day() {
        boolean[] hours = {true, true, true, true, true, true, true};
        assertEquals("Όλη μέρα", Utils.formatHours(hours));
    }

    @Test
    public void format_hours_first_N_hours() {
        boolean[] hours2 = {true, true, false, false, false, false, false};
        assertEquals("Πρώτες δύο ώρες", Utils.formatHours(hours2));

        boolean[] hours3 = {true, true, true, false, false, false, false};
        assertEquals("Πρώτες τρεις ώρες", Utils.formatHours(hours3));

        boolean[] hours4 = {true, true, true, true, false, false, false};
        assertEquals("Πρώτες τέσσερις ώρες", Utils.formatHours(hours4));
    }

    @Test
    public void format_hours_last_N_hours() {
        boolean[] hours2 = {false, false, false, false, false, false, true};
        assertEquals("Τελευταία ώρα", Utils.formatHours(hours2));

        boolean[] hours3 = {false, false, false, false, false, true, true};
        assertEquals("Τελευταίες δύο ώρες", Utils.formatHours(hours3));

        boolean[] hours4 = {false, false, false, false, true, true, true};
        assertEquals("Τελευταίες τρεις ώρες", Utils.formatHours(hours4));
    }

    @Test
    public void format_hours_single_hours() {
        boolean[] hours1 = {true, false, false, false, true, false, false};
        assertEquals("1η, 5η ώρα", Utils.formatHours(hours1));

        boolean[] hours2 = {true, true, false, false, false, true, false};
        assertEquals("1η, 2η, 6η ώρα", Utils.formatHours(hours2));

        boolean[] hours3 = {false, false, false, true, false, false, false};
        assertEquals("4η ώρα", Utils.formatHours(hours3));
    }

    @Test
    public void get_zero_date_time() {
        Date date = new GregorianCalendar(2018, 10, 2, 9, 19).getTime();
        date = Utils.getZeroTimeDate(date);
        assertEquals(0, date.getSeconds());
        assertEquals(0, date.getMinutes());
        assertEquals(0, date.getHours());
    }

    @Test
    public void get_absence_count() {
        boolean hours1[] = new boolean[]{true, false, false, true, false, false, false};
        assertEquals(2, Utils.getAbsenceCount(hours1));

        boolean hours2[] = new boolean[]{false, false, false, false, false, false, false};
        assertEquals(0, Utils.getAbsenceCount(hours2));

        boolean hours3[] = new boolean[]{true, false, true, true, false, true, false};
        assertEquals(4, Utils.getAbsenceCount(hours3));

        boolean hours4[] = new boolean[]{true, true, true, true, true, true, true};
        assertEquals(7, Utils.getAbsenceCount(hours4));
    }


    @Test
    public void format_date() {
        Calendar calendar = new GregorianCalendar(2018, 9, 2);
        assertEquals("Τρίτη, 2 Οκτωβρίου", Utils.formatDate(calendar.getTime()));
    }

    @Test
    public void compare_dates_two_equal() {
        Date a = new Date();
        Date b = new Date();
        assertEquals(0, Utils.compareDatesByDate(a, b));
    }

    @Test
    public void compare_dates_bigger() {
        Date a = new GregorianCalendar(2018, 9, 2).getTime();
        Date b = new GregorianCalendar(2018, 8, 2).getTime();
        assertEquals(-1, Utils.compareDatesByDate(a, b));
    }

    @Test
    public void compare_dates_smaller() {
        Date a = new GregorianCalendar(2018, 9, 1).getTime();
        Date b = new GregorianCalendar(2018, 9, 2).getTime();
        assertEquals(1, Utils.compareDatesByDate(a, b));
    }

    @Test
    public void sort_absenceObjects_by_date() {
        Date a = new GregorianCalendar(2018, 9, 1).getTime();
        Date b = new GregorianCalendar(2018, 9, 2).getTime();
        Date c = new GregorianCalendar(2018, 9, 5).getTime();
        Date d = new GregorianCalendar(2018, 9, 15).getTime();
        ArrayList<AbsenceObject> actual = new ArrayList<>();
        actual.add(absenceObjectWithDate(b));
        actual.add(absenceObjectWithDate(c));
        actual.add(absenceObjectWithDate(d));
        actual.add(absenceObjectWithDate(a));

        ArrayList<AbsenceObject> expected = new ArrayList<>();
        expected.add(actual.get(2));
        expected.add(actual.get(1));
        expected.add(actual.get(0));
        expected.add(actual.get(3));

        Utils.sortByDate(actual);
        assertEquals(expected, actual);
    }

    @Test
    public void sort_absenceObjects_by_count() {
        ArrayList<AbsenceObject> actual = new ArrayList<>();
        actual.add(absenceObject(5));
        actual.add(absenceObject(4));
        actual.add(absenceObject(7));
        actual.add(absenceObject(2));

        ArrayList<AbsenceObject> expected = new ArrayList<>();
        expected.add(actual.get(2));
        expected.add(actual.get(0));
        expected.add(actual.get(1));
        expected.add(actual.get(3));

        Utils.sortByAbsenceCount(actual);
        assertEquals(expected, actual);
    }

    private AbsenceObject absenceObjectWithDate(Date date) {
        AbsenceObject object = mock(AbsenceObject.class);
        when(object.getDate()).thenReturn(date);
        return object;
    }

    private AbsenceObject absenceObject(int absences) {
        AbsenceObject object = mock(AbsenceObject.class);
        when(object.getAbsenceCount()).thenReturn(absences);
        return object;
    }

}